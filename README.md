# Is My Phone Controlling My Life?

My parents always tell me that I am addicted to my phone. I always tell them I am not. How can someone be addicted to a 5.5 inch box of wires and gadgets? There is nothing desirable about the exterior of the phone.

! [iPhone](http://cdn.mos.cms.futurecdn.net/569396121ba176fab9bc4fa4c3361bef.jpg)

We live in a society where phones have become an extension to the body. When your phone is with you, you can _always_ be reached, you are _always_ up to date on what is going on in the world, and you can _always_ communicate.

People have a desire to feel connected. They are willing to sacrifice sleep, sanity and peace to always be in the know. “But what do we really expect from such an intensive, twenty-four / seven connection?” Shaviro asks. Why do we allow the network to control us? Because that is the **norm**. Since people are physically attached to their phones, they are expected to always be involved and respond. It has become an unconditioned response. When you hear the ding or feel the vibration, you can’t help but to check your phone and, in most cases, respond. There is no escape.

! [Notification](https://gigaom.com/wp-content/uploads/sites/1/2014/09/imessage-interactive-notification.jpg) 

The most common form of electronic communication today is texting. A type of communication through shorthand messages. What was once so simple has evolved into a medium that controls many people’s lives. Previous expressions like “c u l8r” and “ttyl” have been replaced with lengthy, grammatically-correct paragraphs. Texting is no longer used for rare, important and urgent messages. It is now a replacement for face-to-face communication. Texting keeps people connected no matter the distance. However, it has reached the point where people in the same room will text each other rather than talk to each other. We have let the network take control.

! [Old texts](http://transl8it.com/wp-content/uploads/2014/09/text-messaging-top-20.jpg) ! [New texts](https://cdn.unlockboot.com/wp-content/uploads/2017/08/imessage-android-fix.jpg)

> “A pattern of information is meaningless by itself . . . [therefore] We cannot think of information as just a pattern imprinted indifferently in one or another physical medium. For information is also an event. It isn’t just the content of a given message but all the things that happen when the message gets transmitted” (Steven Shaviro 16). 

A text has no power or control over us unless we let it. We have the power to turn off our phones or just ignore a message, but we don’t. We voluntarily give up our power and independence to remain connected to the outside world. We want to be involved. We want to be connected. Every time we open a text message and respond, we are letting the network in. Text by text, we are getting more and more trapped in the depths and control of the network. We are simply becoming a part of its system. 

> “You may swat or shoo away a single fly, but more of them will always show up. The media-sphere is the only “nature” we know, and unwanted messages, like insect pests, are a crucial part of its ecology” (Shaviro 23).

You can’t escape. Even if you limit how often you send texts, you cannot control the extent to which the outside world contacts you. Once you are on the grid you cannot be taken off. People will continue to text you until they get a response. They will bombard you with images, texts, videos, blogs, tweets, posts, trends, current events, etc. and there is no end to the harassment. By joining the grid and engaging in texting, you sacrifice your freedom to your phone. Even if you never respond to the texts you receive, the flow of information will not stop and you will always be connected.

! [The Grid](https://ak1.picdn.net/shutterstock/videos/9978161/thumb/9.jpg)

Little did I know that when I bought my phone, I unconsciously signed my independence away in exchange for constant connection. Perhaps my parents _are_ right. Maybe I _am_ addicted to my phone. But if I am addicted to my phone, then everyone else on the grid **must be** addicted, too.
